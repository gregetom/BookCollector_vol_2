actions
save
	| stream dir |
	dir := FileSystem disk workingDirectory.
	stream := (dir / 'saveFile') writeStream.

	"Books"
	BookService instance saveBooksToFile: stream.

	"Publishers"
	PublisherService instance savePublishersToFile: stream.

	"Users"
	UserService instance saveUsersToFile: stream.
	stream close