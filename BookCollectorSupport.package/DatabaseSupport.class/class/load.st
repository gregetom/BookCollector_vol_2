loading
load
	| stream dir line bs ps us |
	dir := FileSystem disk workingDirectory.
	stream := (dir / 'saveFile') readStream.
	bs := BookService instance.
	ps := PublisherService instance.
	us := UserService instance.
	bs removeAllBooks.
	ps removeAllPublishers.
	us removeAllUsers.
	[ (line := stream nextLine) isNil ]
		whileFalse: [ line = '#book'
				ifTrue: [ line := stream nextLine.
					bs loadFromJson: line ]
				ifFalse: [ line = '#publisher'
						ifTrue: [ line := stream nextLine.
							ps loadFromJson: line ]
						ifFalse: [ line = '#user'
								ifTrue: [ line := stream nextLine.
									us loadFromJson: line ] ] ] ].
	stream close