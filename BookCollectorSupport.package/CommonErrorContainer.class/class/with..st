instance creation
with: commonError
	|ce|
	
	ce :=CommonErrorContainer new.
		
	commonError isOk ifTrue: [ 
		ce failureMessage: nil.
		ce successMessage: commonError message.
	] 
	ifFalse: [ 
		ce successMessage: nil.
		ce failureMessage: commonError message.
	].

 ^ ce