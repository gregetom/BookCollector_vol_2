as yet unclassified
alreadyExistsWithObject: anObject withProperty: aProperty
		^ anObject, ' with this ', aProperty, ' already exists'