as yet unclassified
doesNotExistWithObject: anObject withProperty: aProperty
	^ anObject, ' with this ', aProperty, ' does not exist'
