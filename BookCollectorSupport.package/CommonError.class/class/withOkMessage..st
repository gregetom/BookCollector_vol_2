creation
withOkMessage: aString
	|instance|
	instance := CommonError new.
	instance message: aString.
	instance success: true.
	
	^ instance.