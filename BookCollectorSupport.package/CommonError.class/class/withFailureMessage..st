creation
withFailureMessage: aString
	|instance|
	instance := CommonError new.
	instance message: aString.
	instance success: false.
	
	^ instance.