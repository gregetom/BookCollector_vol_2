rendering
renderContentOn: html
	|currentUser mainWindow userService borrowedCopies booksService books|
	
	userService := UserService instance.
	currentUser := userService currentUser.
	
	currentUser isPresent ifTrue: [  
		borrowedCopies := currentUser get borrowedCopies.
		booksService := BookService instance.
		books := booksService books.
	].
	
	mainWindow:=MainWindowUI new.

	
	
	mainWindow renderHeader: html.

	html div 
      class: 'container';
      with: [  
	
	html div 
      class: 'container-fluid text-center'; 
		style: 'margin-top:12rem';
      with: [  
	
	currentUser isPresent ifFalse: [
		html div 
		class: 'col-sm-12 alert alert-danger';
		with: 'You must be logged to see some information' .
	].
		
	
	currentUser isPresent ifTrue: [	
				
		html div 
  	    	class: 'col-sm-12 well'; 
      		with: [ 
					
		html heading level: 2; with: [html strong: 'Books'].
						
			returnBookErrorContainer successMessage ifNotNil: [
					html div 
					class: 'alert alert-success';
					with: returnBookErrorContainer successMessage  .
				].
					
			returnBookErrorContainer failureMessage  ifNotNil: [
					html div 
					class: 'alert alert-danger';
					with: returnBookErrorContainer failureMessage  .
			].
			
			borrowedCopies ifNil: [
				html div 
				class: 'alert alert-success';
				with: 'No borrowed books' .
			].
			
			html div 
   				class: 'col-sm-3'; 
     			with: [  html heading level: 4; with: 
							[html strong: 'Copy number'].
						].
					
	 		html div 
   				class: 'col-sm-3'; 
     			with: [  html heading level: 4; with: 
							[html strong: 'Title'].
						].
		
			html div 
      			class: 'col-sm-3';
      			with: [  html heading level: 4; with: 
							[html strong: 'Author'].
						].

			html div 
     	 		class: 'col-sm-3';
      			with: [  html heading level: 4; with: 
								[html strong: 'Return']. 
						].
					
					
			html break .
			html horizontalRule.
			
			borrowedCopies isEmpty ifTrue: [ 
							html div 
     	 						class: 'col-sm-12 alert alert-warning';
      							with: [	html text: 'No borrowed books' ].
				 ].
			
			borrowedCopies do: [ :each |
							
							html div 
     	 						class: 'col-sm-3';
      							with: [	
										html text: each copyNumber.
									].
								
							html div 
     	 						class: 'col-sm-3';
      							with: [	
										html text: (books at: (each bookNumber)) title.
									].
								
							html div 
     	 						class: 'col-sm-3';
      							with: [	
										html text: (books at: (each bookNumber)) author.
									].
								
									 	
							html div 
     	 						class: 'col-sm-3';
      							with: [	
									html form: [	
											html button 
												class: 'btn btn-primary glyphicon glyphicon-minus btn-block';
												callback: [ self returnBookUI: (each copyNumber) ].
												].
										].
													
									
							html break.
							html break.
							html break.
									
									 ].
								
							].
						].
					].
				].