as yet unclassified
borrowUI: aBookNumber
	|commonError|
	
	commonError :=  BookService instance borrow: aBookNumber.
	
	booksErrorContainer  := CommonErrorContainer with: commonError.