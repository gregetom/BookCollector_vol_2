action
removePublisherUI: aPublisher
	|commonError|
	
	commonError := PublisherService instance removePublisher: aPublisher.
	
	removePublisherErrorContainer := CommonErrorContainer with: commonError.
	
