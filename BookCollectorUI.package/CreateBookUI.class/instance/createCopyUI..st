action
createCopyUI: aCopy
	|commonError|
	
	commonError := BookService instance createCopy: aCopy.
	
	createCopyErrorContainer := CommonErrorContainer with: commonError.