rendering
renderHeader: html
	|currentUser|
	currentUser := UserService instance currentUser.
	
	html div 
		class: 'navbar navbar-inverse';
		style: 'margin-bottom:0;border-radius: 0;position:fixed;width:100%;z-index:5000';
  	   	with: [ 
				html div 
				class: 'container-fluid';
  	   			with: [
						html unorderedList
							class: 'nav navbar-nav';
							with: [
								html listItem: [
									html anchor
									url: '/bookCollector';
									with: 'Home'.
								]. 
								html listItem: [
									html anchor
									url: '/books';
									with: 'Books'.
								].
								currentUser isPresent ifTrue: [
									(currentUser get isAdmin) ifTrue: [
										html listItem: [
											html anchor
											url: '/createBook';
											with: 'Create book'.
										].
									
									html listItem: [
											html anchor
											url: '/publishers';
											with: 'Publishers'.
										].
									].
								].
							].
						
							html unorderedList
							class: 'nav navbar-nav pull-right';
							with: [
								currentUser isPresent ifFalse: [ 
									html listItem: [
										html anchor
										url: 'login';
										with: 'Login'.
										].
									].
								
								html form with:[
									html button
									callback: [ self hardReset  ];
									with:'Reset'
									].
								
								currentUser isPresent ifTrue: [ 
									html listItem: [
										html anchor
										url: 'user';
									with: 'Logged as: ',currentUser get name, ' ', currentUser get surname].
									
								
									html listItem: [
										html anchor
										callback: [ self logoutUI ];
										with: 'Logout'.
										].	
									].
							 
							
							].
						].
					].