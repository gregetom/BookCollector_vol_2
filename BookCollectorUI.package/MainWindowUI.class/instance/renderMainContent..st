rendering
renderMainContent: html
	html div
		class: 'container';
		style: 'padding-bottom:10rem';
		with: [ html div
				class: 'container-fluid text-center';
				with: [ html div
						class: 'col-sm-12';
						with: [ html div
								class: 'col-sm-3';
								with: [ html span
										class: 'glyphicon glyphicon-book';
										style: 'color: #f4511e;font-size: 200px';
										with: [ '' ] ].
							html div
								class: 'col-sm-9';
								style: 'text-align:left';
								with: [ html heading
										level: 1;
										with: [ html strong: 'How to use this application' ].
									html horizontalRule.
									html heading
										level: 4;
										with: [ html strong: 'Show books -> go to "books" in header' ].
									html heading
										level: 4;
										with:
												[ html strong: 'Create book -> go to "create book" in header, fill up inputs and confirm' ].
									html button
										class: 'btn btn-primary btn-block center';
										callback: [ DatabaseSupport save  ];
										with: 'save'.
									html button
										class: 'btn btn-primary btn-block center';
										callback: [ DatabaseSupport load  ];
										with: 'load'.
									html heading
										level: 4;
										with: [ html strong: 'To do dummy text' ] ] ] ] ]