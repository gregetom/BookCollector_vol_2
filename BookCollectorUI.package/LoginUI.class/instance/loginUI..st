action
loginUI: anUser
	|commonError|
	
	commonError := UserService instance login: anUser.
	
	loginErrorContainer := CommonErrorContainer with: commonError.
	
			