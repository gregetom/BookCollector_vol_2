tests
testFindCopy
	|result book|

	book := Book new.
	book bookNumber: '123'.
	book title: 'title'.
	book author: 'author'.
	book genre: 'genre'.
	book publisher: Publisher new.	
	BookService instance findCopy: '123'. 
	
	result := BookService instance findCopy: '123'. 
		
	self assert: result isPresent not.