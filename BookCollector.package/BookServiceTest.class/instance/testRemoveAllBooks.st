tests
testRemoveAllBooks
	|book result service|
	
	service := BookService instance.
	
	book := Book new.
	book bookNumber: '123'.
	book title: 'title'.
	book author: 'author'.
	book genre: 'genre'.
	book publisher: Publisher new.	
	service createBook: book.
	
	self assert: service books size > 0.
	
	result := service removeAllBooks.
		
	self assert: service books size = 0.