tests
testCreateCopyAlreadyExists
	|book copy duplicate result|
	
	book := Book new.
	book bookNumber: '123'.
	book title: 'title'.
	book author: 'author'.
	book genre: 'genre'.
	book publisher: Publisher new.	
	BookService instance createBook: book.
	
	copy:=BookCopy new.
	copy bookNumber: book bookNumber.
	copy copyNumber: '456'.
	BookService instance createCopy: copy.
	
	duplicate :=BookCopy new.
	duplicate bookNumber: book bookNumber.
	duplicate copyNumber: copy copyNumber.
	result := BookService instance createCopy: duplicate.
	
	self assert: result isOk not.