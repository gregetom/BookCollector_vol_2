tests
testCreateBookWithoutPublisher
	|book result|
	book := Book new.
	
	book bookNumber: '123'.
	book title: 'title'.
	book author: 'author'.
	book genre: 'genre'.	
	
	result := BookService instance createBook: book.
	
	self assert: result isOk not.