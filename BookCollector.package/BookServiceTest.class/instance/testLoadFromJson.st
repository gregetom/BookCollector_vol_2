tests
testLoadFromJson
	|result|
	
	result := BookService instance loadFromJson: '
{"author":"test","title":"test","bookNumber":"test","copyList":{},"genre":"test","publisher":{"name":"Albatros"}}
'. 
		
	self assert: result isNil not.