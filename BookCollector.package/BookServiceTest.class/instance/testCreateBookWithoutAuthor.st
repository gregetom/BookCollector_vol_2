tests
testCreateBookWithoutAuthor
	|book result|
	book := Book new.
	
	book bookNumber: '123'.
	book title: 'title'.
	book author: ''.
	book genre: 'genre'.
	book publisher: Publisher new.	
	
	
	result := BookService instance createBook: book.
	
	self assert: result isOk not.