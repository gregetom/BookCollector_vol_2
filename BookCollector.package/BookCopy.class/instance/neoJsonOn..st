as yet unclassified
neoJsonOn: neoJsonWriter
	
	neoJsonWriter for: BookCopy do: [ :mapping |
	mapping mapInstVars: #(bookNumber copyNumber borrowed) ].
	
	neoJsonWriter writeObject: self 