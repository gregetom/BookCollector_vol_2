as yet unclassified
neoJsonOn: neoJsonWriter

	neoJsonWriter for: User do: [ :mapping | 
		mapping mapInstVars: #(name surname role email password borrowedCopies)  ].

	neoJsonWriter writeObject: self.