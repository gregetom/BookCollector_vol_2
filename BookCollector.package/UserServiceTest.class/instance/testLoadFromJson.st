tests
testLoadFromJson
	|result|
	
	result := UserService instance loadFromJson: '{"name":"User","surname":"Userovski","role":"user","email":"user@user","password":"user","borrowedCopies":{}}'. 
		
	self assert: result isNil not.
	self assert: result name equals: 'User'.