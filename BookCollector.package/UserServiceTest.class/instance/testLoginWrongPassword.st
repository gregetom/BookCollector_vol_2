tests
testLoginWrongPassword
 |user logingUser result|
	
	user:=User new.
	user name: 'name'.
	user surname: 'surname'.
	user email: 'email'.
	user password: 'password'.
	UserService instance registerUser: user.
	
	logingUser := User new.
	logingUser email: user email.
	logingUser password: 'wrong password'.

	result :=UserService instance login: user.

	self assert: result isOk not.



