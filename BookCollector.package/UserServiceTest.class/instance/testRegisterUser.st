tests
testRegisterUser
 |size user instance result|
	instance:= UserService instance.

	user:=User new.
	user name: 'name'.
	user surname: 'surname'.
	user email: 'email'.
	user password: 'password'.
	 
	size:= instance users size.

	result :=instance registerUser: user.

	self assert: result isOk.
	self assert: instance users size = (size+1).


