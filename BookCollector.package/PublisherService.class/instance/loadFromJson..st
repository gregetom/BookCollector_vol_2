as yet unclassified
loadFromJson: aString
	self
		register:
			((NeoJSONReader on: aString readStream)
				mapInstVarsFor: Publisher;
				nextAs: Publisher)