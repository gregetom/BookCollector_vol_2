adding
addPublisher: aPublisher
	aPublisher name ifEmpty: [ ^ CommonError withFailureMessage:(MessagesSupport cannotBeNullOrEmpty: 'Name') ].
	(publishers includesKey: aPublisher name)
		ifTrue: [ ^ CommonError withFailureMessage: (MessagesSupport alreadyExistsWithObject: 'Publisher' withProperty:'name' ) ].
	self register: aPublisher.
	DatabaseSupport save.
	^ CommonError withOkMessage: MessagesSupport successfullyDone.