removing
removePublisher: aPublisher
	| currentUser |
	currentUser := UserService instance currentUser.

	(publishers includesKey: aPublisher name)
		ifFalse: [ ^ CommonError withFailureMessage: (MessagesSupport doesNotExistWithObject: 'Publisher' withProperty: 'name' ) ].

	publishers removeKey: aPublisher name.
	DatabaseSupport save.
	^ CommonError withOkMessage: MessagesSupport successfullyDone.