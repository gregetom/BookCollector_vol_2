as yet unclassified
neoJsonOn: neoJsonWriter
	neoJsonWriter
		for: Book
		do: [ :mapping | mapping mapInstVars: #(author title bookNumber copyList genre publisher) ].
	neoJsonWriter writeObject: self