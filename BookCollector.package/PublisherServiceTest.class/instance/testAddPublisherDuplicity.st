tests
testAddPublisherDuplicity
	|result publisher duplicity|
	
	publisher := Publisher new.
	publisher name: 'name'.
	PublisherService instance addPublisher: publisher.
	
	duplicity := Publisher new.
	duplicity name: 'name'.
		
	result := PublisherService instance addPublisher: duplicity.
	
	self assert: result isOk not.
