tests
testAddPublisher
	|result publisher|
	
	publisher := Publisher new.
	publisher name: 'name'.
	
	result := PublisherService instance addPublisher: publisher.
	
	self assert: result isOk.
