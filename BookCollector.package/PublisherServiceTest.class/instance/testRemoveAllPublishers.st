tests
testRemoveAllPublishers
	|publisher service result|
	service:=PublisherService instance.
	
	publisher := Publisher new.
	publisher name: 'name'.
	
	self assert: service publishers size > 0.
	
	result := service removeAllPublishers. 
		
	self assert: service publishers size = 0.
