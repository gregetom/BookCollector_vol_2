tests
testAddPublisherWithoutName
	|result publisher|
	
	publisher := Publisher new.
	publisher name: ''.
	
	result := PublisherService instance addPublisher: publisher.
	
	self assert: result isOk not.
