as yet unclassified
saveUsersToFile: aStream
	users
		do: [ :each | 
			aStream nextPutAll: '#user'.
			aStream nextPut: Character cr.
			aStream nextPutAll: (NeoJSONWriter toString: each).
			aStream nextPut: Character cr. ]