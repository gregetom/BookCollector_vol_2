initializing
initialize
	|admin user|
	
	super initialize.
	users := Dictionary new.
	
	admin := User new.
	admin name: 'Admin'.
	admin surname: 'Adminovski'.
	admin email: 'admin@admin'.
	admin password: 'admin'.
	admin role: 'admin'.
	admin borrowedCopies: Dictionary new.
	
	user := User new.
	user name: 'User'.
	user surname: 'Userovski'.
	user email: 'user@user'.
	user password: 'user'.
	user role: 'user'.
	user borrowedCopies: Dictionary new.
	
	users at: 'admin' put: admin.
	users at: 'user' put: user.
	
