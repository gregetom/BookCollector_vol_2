as yet unclassified
login: anUser
	|tmpUser|
	
	anUser email ifEmpty: [ ^ CommonError withFailureMessage: (MessagesSupport cannotBeNullOrEmpty: 'Email')  ].
	anUser password ifEmpty: [  ^ CommonError withFailureMessage: (MessagesSupport cannotBeNullOrEmpty: 'Password') ].
	
	(users includesKey: anUser email) ifFalse: 
				[  ^ CommonError withFailureMessage: (MessagesSupport doesNotExistWithObject: 'User' withProperty: 'username')].
	
	tmpUser := users at: (anUser email).
	
	((tmpUser password) = (anUser password))	
	ifTrue: [ 
			currentUser := tmpUser.
		 ^ CommonError withOkMessage: MessagesSupport successfullyLogged
		 ].

	currentUser := nil.
	 ^ CommonError withFailureMessage: MessagesSupport invalidPassword.