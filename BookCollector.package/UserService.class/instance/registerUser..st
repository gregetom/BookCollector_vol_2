as yet unclassified
registerUser: aUser
	aUser name ifEmpty: [ ^ CommonError withFailureMessage: (MessagesSupport cannotBeNullOrEmpty: 'Name') ].
	aUser surname ifEmpty: [ ^ CommonError withFailureMessage: (MessagesSupport cannotBeNullOrEmpty: 'Surname')  ].
	aUser email ifEmpty: [ ^ CommonError withFailureMessage: (MessagesSupport cannotBeNullOrEmpty: 'Email') ].
	aUser password ifEmpty: [ ^ CommonError withFailureMessage: (MessagesSupport cannotBeNullOrEmpty: 'Password') ].
	(users includesKey: aUser email)
		ifTrue: [ ^ CommonError withFailureMessage: (MessagesSupport alreadyExistsWithObject: 'User' withProperty: 'email') ].
	aUser role: 'user'.
	self register: aUser.
	DatabaseSupport save.
	^ CommonError withOkMessage: MessagesSupport successfullyDone.