as yet unclassified
saveBooksToFile: aStream
	books
		do: [ :each | 
			aStream nextPutAll: '#book'.
			aStream nextPut: Character cr.
			aStream nextPutAll: (NeoJSONWriter toString: each).
			aStream nextPut: Character cr ]