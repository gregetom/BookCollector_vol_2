private
createCopy: aCopy
	| book copy |
	aCopy copyNumber isEmpty
		ifTrue: [ ^ CommonError withFailureMessage: (MessagesSupport cannotBeNullOrEmpty: 'Copy number') ].
	(books includesKey: aCopy bookNumber)
		ifFalse: [ ^ CommonError withFailureMessage: (MessagesSupport mustExistsWithObject: 'Book' withProperty: 'book number') ].
	copy := self findCopy: aCopy copyNumber.
	copy isPresent
		ifTrue: [ ^ CommonError withFailureMessage: (MessagesSupport alreadyExistsWithObject: 'Copy' withProperty:  'copy number') ].
	book := books at: aCopy bookNumber.
	book copyList at: aCopy copyNumber put: aCopy.

	DatabaseSupport save.
	^ CommonError withOkMessage: MessagesSupport successfullyDone.