as yet unclassified
returnBook: aCopyNumber
	| userService currentUser copy |
	userService := UserService instance.
	currentUser := userService currentUser.
	
	aCopyNumber ifEmpty: [ ^ CommonError withFailureMessage: (MessagesSupport cannotBeNullOrEmpty: 'Copy number') ].
	copy := self findCopy: aCopyNumber.
	copy isPresent
		ifFalse: [ ^ CommonError withFailureMessage: (MessagesSupport doesNotExistWithObject: 'Copy' withProperty:  'number')].
	
	copy get borrowed: false.
	userService currentUser get borrowedCopies removeKey: aCopyNumber.
	DatabaseSupport save.
	^ CommonError withOkMessage: MessagesSupport successfullyDone.