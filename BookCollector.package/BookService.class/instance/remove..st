removing
remove: aBookNumber
	| availableCopyList |
	
	(books includesKey: aBookNumber)
		ifFalse: [ ^ CommonError withFailureMessage: (MessagesSupport doesNotExistWithObject: 'Book' withProperty: 'number' ) ].
	availableCopyList := (books at: aBookNumber) availableCopies.
	availableCopyList size = (books at: aBookNumber) copyList size
		ifFalse: [ ^ CommonError withFailureMessage: MessagesSupport cannotDeleteBorrowedBook ].
	books removeKey: aBookNumber.
	DatabaseSupport save.
	^ CommonError withOkMessage: MessagesSupport successfullyDone.