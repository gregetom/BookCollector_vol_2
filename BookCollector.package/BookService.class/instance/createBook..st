private
createBook: aBook

	aBook bookNumber isNil | aBook bookNumber isEmpty
		ifTrue: [ ^ CommonError withFailureMessage: (MessagesSupport cannotBeNullOrEmpty: 'Book number') ].
	aBook title isNil | aBook title isEmpty
		ifTrue: [ ^ CommonError withFailureMessage: (MessagesSupport cannotBeNullOrEmpty: 'Title') ].
	aBook author isNil | aBook author isEmpty
		ifTrue: [ ^ CommonError withFailureMessage: (MessagesSupport cannotBeNullOrEmpty: 'Author') ].
	aBook publisher ifNil: [ ^ CommonError withFailureMessage: (MessagesSupport cannotBeNullOrEmpty: 'Publisher') ].
	(books includesKey: aBook bookNumber)
		ifTrue: [ ^ CommonError withFailureMessage: (MessagesSupport alreadyExistsWithObject: 'Book' withProperty: 'number') ].
	self register: aBook.
	DatabaseSupport save.
	^ CommonError withOkMessage: MessagesSupport successfullyDone.